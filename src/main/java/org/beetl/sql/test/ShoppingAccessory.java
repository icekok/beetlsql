package org.beetl.sql.test;

import java.util.Date;

public class ShoppingAccessory  {
	
	private String id ;
	private String albumId ;
	private String areaCode ;
	private String configId ;
	private String deletestatus ;
	private String ext ;
	private Long height ;
	private String info ;
	private String name ;
	private String newsdata ;
	private String path ;
	private Float size ;
	private String userId ;
	private Long version ;
	private Long width ;
	private String zoneCode ;
	private Date addtime ;
	
	public ShoppingAccessory() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public String getAlbumId(){
		return  albumId;
	}
	public void setAlbumId(String albumId ){
		this.albumId = albumId;
	}
	
	public String getAreaCode(){
		return  areaCode;
	}
	public void setAreaCode(String areaCode ){
		this.areaCode = areaCode;
	}
	
	public String getConfigId(){
		return  configId;
	}
	public void setConfigId(String configId ){
		this.configId = configId;
	}
	
	public String getDeletestatus(){
		return  deletestatus;
	}
	public void setDeletestatus(String deletestatus ){
		this.deletestatus = deletestatus;
	}
	
	public String getExt(){
		return  ext;
	}
	public void setExt(String ext ){
		this.ext = ext;
	}
	
	public Long getHeight(){
		return  height;
	}
	public void setHeight(Long height ){
		this.height = height;
	}
	
	public String getInfo(){
		return  info;
	}
	public void setInfo(String info ){
		this.info = info;
	}
	
	public String getName(){
		return  name;
	}
	public void setName(String name ){
		this.name = name;
	}
	
	public String getNewsdata(){
		return  newsdata;
	}
	public void setNewsdata(String newsdata ){
		this.newsdata = newsdata;
	}
	
	public String getPath(){
		return  path;
	}
	public void setPath(String path ){
		this.path = path;
	}
	
	public Float getSize(){
		return  size;
	}
	public void setSize(Float size ){
		this.size = size;
	}
	
	public String getUserId(){
		return  userId;
	}
	public void setUserId(String userId ){
		this.userId = userId;
	}
	
	public Long getVersion(){
		return  version;
	}
	public void setVersion(Long version ){
		this.version = version;
	}
	
	public Long getWidth(){
		return  width;
	}
	public void setWidth(Long width ){
		this.width = width;
	}
	
	public String getZoneCode(){
		return  zoneCode;
	}
	public void setZoneCode(String zoneCode ){
		this.zoneCode = zoneCode;
	}
	
	public Date getAddtime(){
		return  addtime;
	}
	public void setAddtime(Date addtime ){
		this.addtime = addtime;
	}
	

}
